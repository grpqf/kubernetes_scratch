# Kubernetes The Hard Way

## Objetivo
Fazer a configuracao do cluster multimaster manualmente

## Fases
- Fazer todo o processo via script
- Converter os scripts em roles ansible
- Converter ansible em go ou rust

### Ciclo de Vida

1.   Entidade Certificadora

    1.1 Instalacao de ferramentas para configuracao e geracao da Entidade Certificadora
    1.2 Populando arquivo para configuração da Entidade Certificadora
    1.3 Populando informacoes referentes à Entidade Certificadora
    1.4 Gerando CA
    
    
