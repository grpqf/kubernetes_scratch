#!/bin/bash

PATH_CERTS=/opt/kubernetes/certs
PATH_CLIENT=$PATH_CERTS/client
PATH_CA=$PATH_CLIENT/ca

WORKER1_HOST=worker1.k8s.qualityfactory.com.br
WORKER1_IP=192.128.10.5
WORKER2_HOST=worker2.k8s.qualityfactory.com.br
WORKER2_IP=192.128.10.6

cd /opt/kubernetes/certs/workers/kubelet

#worker 1
echo "Gerando certificado do kubelet para o worker ${WORKER1_HOST}"

cat > ${WORKER1_HOST}-csr.json << EOF
{
   "CN": "system:node:${WORKER1_HOST}",
   "key": {
     "algo": "rsa",
     "size": 2048
    },
   "names": [
     {
       "C": "US",
       "L": "Portland",
       "O": "system:nodes",
       "OU": "Kubernetes The Hard Way",
       "ST": "Oregon"
     }
   ]
}
EOF

cfssl gencert -ca=${PATH_CA}/ca.pem -ca-key=${PATH_CA}/ca-key.pem -config=${PATH_CA}/ca-config.json -hostname=${WORKER1_IP},${WORKER1_HOST} -profile=kubernetes ${WORKER1_HOST}-csr.json | cfssljson -bare ${WORKER1_HOST}


#worker 2
echo ""Gerando certificado do kubelet para o worker ${WORKER2_HOST}

cat > ${WORKER2_HOST}-csr.json << EOF
{
   "CN": "system:node:${WORKER2_HOST}",
   "key": {
     "algo": "rsa",
     "size": 2048
    },
   "names": [
     {
       "C": "US",
       "L": "Portland",
       "O": "system:nodes",
       "OU": "Kubernetes The Hard Way",
       "ST": "Oregon"
     }
   ]
}
EOF

cfssl gencert -ca=${PATH_CA}/ca.pem -ca-key=${PATH_CA}/ca-key.pem -config=${PATH_CA}/ca-config.json -hostname=${WORKER2_IP},${WORKER2_HOST} -profile=kubernetes ${WORKER2_HOST}-csr.json | cfssljson -bare ${WORKER2_HOST}
