#!/bin/bash

PATH_CERTS=/opt/kubernetes/certs
PATH_CA=$PATH_CERTS/client/ca
PATH_MASTER=$PATH_CERTS/master
CERT_HOSTNAME=10.32.0.1,192.128.10.2,master1.k8s.qualityfactory.com.br,192.128.10.3,master2.k8s.qualityfactory.com.br,192.128.10.10,lb.k8s.qualityfactory.com.br,127.0.0.1,localhost,kubernetes.default

echo "Gerando certificado para API Server"

cd $PATH_MASTER

cat > kubernetes-csr.json << EOF 
{
    "CN": "kubernetes",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
      {
          "C": "US",
          "L": "Portland",
          "O": "Kubernetes",
          "OU": "Kubernetes The Hard Way",
          "ST": "Oregon"
      }
    ]
}
EOF

cfssl gencert -ca=${PATH_CA}/ca.pem -ca-key=${PATH_CA}/ca-key.pem -config=${PATH_CA}/ca-config.json -hostname=${CERT_HOSTNAME} -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes
