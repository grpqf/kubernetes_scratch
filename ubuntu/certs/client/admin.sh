#!/bin/bash

PATH_CERTS=/opt/kubernetes/certs
PATH_CLIENT=$PATH_CERTS/client
PATH_CA=$PATH_CLIENT/ca
PATH_ADMIN=$PATH_CLIENT/admin

cd $PATH_ADMIN

echo "Gerando certificados para admin"

cat > admin-csr.json << EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert -ca=$PATH_CA/ca.pem -ca-key=$PATH_CA/ca-key.pem -config=$PATH_CA/ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin

