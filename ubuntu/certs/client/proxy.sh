#!/bin/bash

echo "Gerando certificado para o proxy"

PATH_CERTS=/opt/kubernetes/certs
PATH_CA=$PATH_CERTS/client/ca
PATH_PROXY=$PATH_CERTS/client/proxy

cd $PATH_PROXY

cat > kube-proxy-csr.json << EOF
{
    "CN": "system:kube-proxy",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "US",
            "L": "Portland",
            "O": "system:node-proxier",
            "OU": "Kubernetes The Hard Way",
            "ST": "Oregon"
        }
    ]
}
EOF

cfssl gencert -ca=${PATH_CA}/ca.pem -ca-key=${PATH_CA}/ca-key.pem -config=$PATH_CA/ca-config.json -profile=kubernetes kube-proxy-csr.json | cfssljson -bare kube-proxy 