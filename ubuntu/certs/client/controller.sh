#!/bin/bash

PATH_CERTS=/opt/kubernetes/certs
PATH_CA=$PATH_CERTS/client/ca
PATH_CONTROLLER=$PATH_CERTS/client/controller 

cd $PATH_CONTROLLER

echo "Gerando certificado para o controller"

cat > kube-controller-manager-csr.json << EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert -ca=$PATH_CA/ca.pem -ca-key=$PATH_CA/ca-key.pem -config=$PATH_CA/ca-config.json -profile=kubernetes kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager

