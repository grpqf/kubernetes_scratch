#!/bin/bash

echo "Gerando certificado para o scheduler"

PATH_CERTS=/opt/kubernetes/certs
PATH_CA=$PATH_CERTS/client/ca
PATH_SCHEDULER=$PATH_CERTS/client/scheduler

cd $PATH_SCHEDULER

cat > kube-scheduler-csr.json << EOF
{
    "CN": "system:kube-scheduler",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "US",
            "L": "Portland",
            "O": "system:kube-scheduler",
            "OU": "Kubernetes The Hard Way",
            "ST": "Oregon"
        }
    ]
}
EOF

cfssl gencert -ca=${PATH_CA}/ca.pem -ca-key=${PATH_CA}/ca-key.pem -config=${PATH_CA}/ca-config.json -profile=kubernetes kube-scheduler-csr.json | cfssljson -bare kube-scheduler