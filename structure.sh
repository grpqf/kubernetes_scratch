#!/bin/bash

PATH_CERTS=/opt/kubernetes/certs
PATH_CLIENT=$PATH_CERTS/client
PATH_CA=$PATH_CLIENT/ca
PATH_ADMIN=$PATH_CLIENT/admin
PATH_WORKERS=$PATH_CERTS/workers/kubelet
PATH_CONTROLLER=$PATH_CLIENT/controller 
PATH_PROXY=$PATH_CLIENT/proxy
PATH_SCHEDULER=$PATH_CLIENT/scheduler 
PATH_MASTER=$PATH_CERTS/master

echo "Gerando estrutura para manter os certificados"
mkdir -p $PATH_CERTS

echo "Gerando estrutura para manter os certificados client"
mkdir -p $PATH_CLIENT

echo "Gerando estrutura para manter os certificados para o CA"
mkdir $PATH_CA

echo "Gerando estrutura para manter os certificados para o admin"
mkdir $PATH_ADMIN

echo "Gerando estrutura para manter os certificados do kubelet para os workers"
mkdir -p $PATH_WORKERS

echo "Gerando estrutura para manter os certificados para o controller"
mkdir $PATH_CONTROLLER

echo "Gerando estrutura para manter os certificados para o proxy"
mkdir $PATH_PROXY

echo "Gerando estrutura para manter os certificados para o scheduler"
mkdir $PATH_SCHEDULER

echo "Gerando estrutura para manter os certificados para o API Server"
mkdir $PATH_MASTER